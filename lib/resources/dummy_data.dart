import 'package:car_rent_preview/models/brand_model.dart';
import 'package:car_rent_preview/models/car_model.dart';

class DummyData {

  static final List<String> _descriptions = [
    'The Tesla Model S is a full-size all-electric five-door liftback produced by Tesla, Inc. It was introduced in June 2012 and is currently the longest-range electric vehicle in production. The Model S has several features, including Autopilot driver assistance, over-the-air software updates to improve performance, range, and interior features and design, plus the option of Ludicrous Mode which allows the car to accelerate from 0–60 mph in 2.8 seconds.',
    'The BMW X4 is a Sports Activity Coupe (SAC) produced by BMW. It was introduced in 2014 and is based on the same chassis as the BMW X3. The X4 is a crossover vehicle, offering a combination of coupe styling with the practicality of an SUV. The X4 is powered by a range of petrol and diesel engines, and is available with four-wheel drive and automatic transmission options. It features an 8-speed automatic gearbox as standard, as well as a choice of Sport, Comfort and EcoPro driving modes to tailor the driving experience to the driver.',
    'The Mercedes AMG GT is a high-performance sports car manufactured by Mercedes-AMG. It was introduced in 2014 and is available as a coupe or roadster. The AMG GT features a 4.0L twin-turbocharged V8 engine that produces up to 577 horsepower and can accelerate from 0 to 60 mph in just 3.6 seconds. The interior of the vehicle is customisable with various options such as real leather seating, carbon fiber accents, and a Nappa leather wrapped steering wheel. Other features include an adjustable suspension system, forged alloy wheels, and a composite carbon fiber roof.'
  ];

  static List<BrandModel> brands = [
    BrandModel(id: 1, name: 'Tesla', icon: 'tesla.png'),
    BrandModel(id: 2, name: 'Mercedes', icon: 'mercedes.png'),
    BrandModel(id: 3, name: 'BMW', icon: 'bmw.png'),
    BrandModel(id: 4, name: 'Volkswagen', icon: 'volkswagen.png'),
    BrandModel(id: 4, name: 'Suzuki', icon: 'suzuki.png'),
    BrandModel(id: 4, name: 'Mini Cooper', icon: 'cooper.png'),
  ];

  static List<CarModel> cars = [
    CarModel(id: 1, name: 'Tesla Model S', icon: 'tesla_model_s.png', rentPrice: 210.50, brand: brands[0], ratings: 4.5, description: _descriptions[0]),
    CarModel(id: 1, name: 'Mercedes AMG GT ', icon: 'mercedes_amg_gt.png', rentPrice: 310.50, brand: brands[0], ratings: 4.6, description: _descriptions[2]),
    CarModel(id: 1, name: 'BMW X4', icon: 'bmw_x4.png', rentPrice: 110.50, brand: brands[0], ratings: 4.8, description: _descriptions[1]),
  ];
}
