import 'package:car_rent_preview/routes/AppRoutes.dart';
import 'package:car_rent_preview/views/screens/car_detail_screen.dart';
import 'package:car_rent_preview/views/screens/home_screen.dart';
import 'package:go_router/go_router.dart';

class AppPage {
  static var routes = GoRouter(
    initialLocation: AppRoutes.HOME,
    routes: [
      GoRoute(
        path: AppRoutes.HOME,
        builder: (context, state) => const HomeScreen(),
      ),
      GoRoute(
        path: AppRoutes.DETAILS,
        builder: (context, state) => const CarDetailScreen(),
      )
    ],
  );
}
