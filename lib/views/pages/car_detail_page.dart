import 'package:car_rent_preview/helper/color_helper.dart';
import 'package:car_rent_preview/models/car_model.dart';
import 'package:car_rent_preview/resources/dummy_data.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CarDetailPage extends StatelessWidget {
  const CarDetailPage({super.key});

  @override
  Widget build(BuildContext context) {
    CarModel car = DummyData.cars[1];
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return ListView(
      physics: const BouncingScrollPhysics(),
      children: [
        SizedBox(
          height: heightSize * 0.4,
          child: Image.asset('assets/images/${car.icon}', fit: BoxFit.fitWidth),
        ),
        Container(
          padding: EdgeInsets.only(left: widthSize * 0.05, right: widthSize * 0.05),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(car.name, style: TextStyle(
                color:  ColorHelper.white,
                fontSize: heightSize * 0.03,
                fontWeight: FontWeight.w600
              )),
              Text('\$${car.rentPrice.toStringAsFixed(2)} / Day', style: TextStyle(
                  color:  ColorHelper.primary,
                  fontSize: heightSize * 0.02,
                  fontWeight: FontWeight.w600
              ))
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.only(left: widthSize * 0.05, right: widthSize * 0.05),
          margin: EdgeInsets.only(top: heightSize * 0.03, bottom: heightSize * 0.03),
          child: Row(
            children: [
              Icon(Icons.star_rate, size: 16, color: ColorHelper.primary,),
              Container(
                margin: EdgeInsets.only(left: widthSize * 0.01, right: widthSize * 0.02),
                child: Text(car.ratings.toStringAsFixed(2), style: TextStyle(
                    color:  ColorHelper.white,
                    fontWeight: FontWeight.w600
                )),
              ),
              Text('(1.4k Reviewed)', style: TextStyle(
                  color:  ColorHelper.white.withOpacity(0.5),
                  fontWeight: FontWeight.w400
              )),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.only(left: widthSize * 0.05, right: widthSize * 0.05),
          child: Text(car.description, textAlign: TextAlign.justify, maxLines: 10, style: TextStyle(
            height: 1.5,
            color: ColorHelper.white.withOpacity(0.5)
          )),
        ),
        Container(
          padding: EdgeInsets.only(left: widthSize * 0.05, right: widthSize * 0.05),
          margin: EdgeInsets.only(top: heightSize * 0.05),
          child: Container(
            padding: EdgeInsets.only(top: heightSize * 0.025, bottom: heightSize * 0.025),
              decoration: BoxDecoration(
                  color: ColorHelper.primary,
                borderRadius: BorderRadius.circular(15)
              ),
            child: Center(
              child: Text('Book a Car',style: TextStyle(
                color: ColorHelper.white,
                fontWeight: FontWeight.w600
              )),
            )
          ),
        )
      ],
    );
  }
}
