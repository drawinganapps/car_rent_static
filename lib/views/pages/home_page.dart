import 'package:car_rent_preview/helper/color_helper.dart';
import 'package:car_rent_preview/resources/dummy_data.dart';
import 'package:car_rent_preview/routes/AppRoutes.dart';
import 'package:car_rent_preview/views/widgets/brand_card_widget.dart';
import 'package:car_rent_preview/views/widgets/car_card_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return ListView(
      physics: const BouncingScrollPhysics(),
      children: [
        Container(
          margin: EdgeInsets.only(
              top: heightSize * 0.03, bottom: heightSize * 0.03),
          padding:
              EdgeInsets.only(left: widthSize * 0.05, right: widthSize * 0.05),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Easily Find Your',
                  style: TextStyle(
                      color: ColorHelper.white,
                      fontSize: 36,
                      fontWeight: FontWeight.w800)),
              Text('Reent Car',
                  style: TextStyle(
                      color: ColorHelper.primary,
                      fontSize: 36,
                      fontWeight: FontWeight.w800))
            ],
          ),
        ),
        SizedBox(
          height: heightSize * 0.06,
          child: ListView(
            scrollDirection: Axis.horizontal,
            physics: const BouncingScrollPhysics(),
            children: List.generate(DummyData.brands.length, (index) {
              return Container(
                margin: EdgeInsets.only(left: widthSize * 0.05),
                child: BrandCardWidget(
                  brand: DummyData.brands[index],
                  isSelected: index == 0,
                ),
              );
            }),
          ),
        ),
        Container(
          margin: EdgeInsets.only(
              top: heightSize * 0.03, bottom: heightSize * 0.03),
          padding:
              EdgeInsets.only(left: widthSize * 0.05, right: widthSize * 0.05),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text('Luxury Car',
                  style: TextStyle(
                      color: ColorHelper.white,
                      fontWeight: FontWeight.w600,
                      fontSize: 18)),
              const Icon(Icons.arrow_right_alt)
            ],
          ),
        ),
        SizedBox(
            height: heightSize * 0.5,
            child: ListView(
              scrollDirection: Axis.horizontal,
              physics: const BouncingScrollPhysics(),
              children: List.generate(DummyData.cars.length, (index) {
                return Container(
                  width: widthSize * 0.6,
                  margin: EdgeInsets.only(left: widthSize * 0.05),
                  child: GestureDetector(
                    child: CarCardWidget(car: DummyData.cars[index]),
                    onTap: () => context.go(AppRoutes.DETAILS),
                  ),
                );
              }),
            ))
      ],
    );
  }
}
