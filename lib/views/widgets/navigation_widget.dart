import 'package:car_rent_preview/helper/color_helper.dart';
import 'package:flutter/material.dart';

class NavigationWidget extends StatelessWidget {
  final int selectedMenu;

  const NavigationWidget({super.key, required this.selectedMenu});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return Container(
      decoration: BoxDecoration(
          color: ColorHelper.lightDark,
          borderRadius: const BorderRadius.only(topLeft: Radius.circular(25), topRight: Radius.circular(25))),
      padding:
      EdgeInsets.only(left: widthSize * 0.03, right: widthSize * 0.03),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            decoration: const BoxDecoration(shape: BoxShape.circle),
            padding: EdgeInsets.all(heightSize * 0.015),
            child: Icon(Icons.directions_car_rounded,
                size: selectedMenu == 0 ? 45 : 35,
                color: selectedMenu == 0
                    ? ColorHelper.primary
                    : ColorHelper.white),
          ),
          Container(
            decoration: const BoxDecoration(shape: BoxShape.circle),
            padding: EdgeInsets.all(heightSize * 0.015),
            child: Icon(Icons.search,
                size: selectedMenu == 1 ? 45 : 35,
                color: selectedMenu == 1
                    ? ColorHelper.primary
                    : ColorHelper.white),
          ),
          Container(
            decoration: const BoxDecoration(shape: BoxShape.circle),
            padding: EdgeInsets.all(heightSize * 0.015),
            child: Icon(Icons.favorite_outline,
                size: selectedMenu == 2 ? 45 : 35,
                color: selectedMenu == 2
                    ? ColorHelper.primary
                    : ColorHelper.white),
          ),
          Container(
            decoration: const BoxDecoration(shape: BoxShape.circle),
            padding: EdgeInsets.all(heightSize * 0.015),
            child: Icon(Icons.person_outline,
                size: selectedMenu == 4 ? 45 : 35,
                color: selectedMenu == 4
                    ? ColorHelper.primary
                    : ColorHelper.white),
          ),
        ],
      ),
    );
  }
}
