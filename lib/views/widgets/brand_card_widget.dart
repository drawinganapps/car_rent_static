import 'package:car_rent_preview/helper/color_helper.dart';
import 'package:car_rent_preview/models/brand_model.dart';
import 'package:flutter/cupertino.dart';

class BrandCardWidget extends StatelessWidget {
  final BrandModel brand;
  final bool isSelected;

  const BrandCardWidget({super.key, required this.brand , required this.isSelected});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: isSelected ? ColorHelper.primary : ColorHelper.lightDark,
        borderRadius: BorderRadius.circular(10)
      ),
      child: Image.asset('assets/icons/${brand.icon}', fit: BoxFit.cover),
    );
  }
}
