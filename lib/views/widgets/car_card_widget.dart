import 'package:car_rent_preview/helper/color_helper.dart';
import 'package:car_rent_preview/models/car_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CarCardWidget extends StatelessWidget {
  final CarModel car;

  const CarCardWidget({super.key, required this.car});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return Container(
      padding:
          EdgeInsets.only(top: heightSize * 0.03, bottom: heightSize * 0.03),
      decoration: BoxDecoration(
        color: ColorHelper.lightDark,
        borderRadius: BorderRadius.circular(15)
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(
                left: widthSize * 0.05, right: widthSize * 0.05),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(Icons.star_rate, size: 16, color: ColorHelper.primary,),
                Text('${car.ratings}',
                    style: TextStyle(
                        color: ColorHelper.white, fontWeight: FontWeight.w600)),
              ],
            ),
          ),
          Image.asset('assets/images/${car.icon}', fit: BoxFit.cover),
          Container(
            padding: EdgeInsets.only(
                left: widthSize * 0.05, right: widthSize * 0.05),
            child: Text('${car.description}',
                maxLines: 6,
                textAlign: TextAlign.justify,
                style: TextStyle(
                    color: ColorHelper.white, overflow: TextOverflow.fade)),
          ),
          Container(
            padding: EdgeInsets.only(
                left: widthSize * 0.05, right: widthSize * 0.05),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('${car.name}',
                    style: TextStyle(
                        color: ColorHelper.white,
                        fontWeight: FontWeight.w600,
                        fontSize: 20)),
                Container(
                  margin: EdgeInsets.only(top: heightSize * 0.01),
                ),
                Text('\$${car.rentPrice.toStringAsFixed(2)} / Day',
                    style: TextStyle(
                        color: ColorHelper.primary,
                        fontWeight: FontWeight.w500,
                        fontSize: 18)),
              ],
            ),
          )
        ],
      ),
    );
  }
}
