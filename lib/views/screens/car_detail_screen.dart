import 'package:car_rent_preview/helper/color_helper.dart';
import 'package:car_rent_preview/routes/AppRoutes.dart';
import 'package:car_rent_preview/views/pages/car_detail_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class CarDetailScreen extends StatelessWidget {
  const CarDetailScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorHelper.dark,
        elevation: 0,
        leadingWidth: widthSize * 0.2,
        leading: Center(
            child: GestureDetector(
              onTap: () => context.go(AppRoutes.HOME),
              child: Container(
                padding: const EdgeInsets.all(7),
                decoration: BoxDecoration(
                    color: ColorHelper.lightDark, shape: BoxShape.circle),
                child: Icon(Icons.arrow_back, color: ColorHelper.white),
              ),
            )),
        centerTitle: true,
        title: Text('Details',
            style: TextStyle(
                color: ColorHelper.white,
                fontSize: 20,
                fontWeight: FontWeight.w600)),
        actions: [
          Container(
            margin: EdgeInsets.only(right: widthSize * 0.05),
            child: Center(
              child: Container(
                padding: const EdgeInsets.all(7),
                decoration: BoxDecoration(
                    shape: BoxShape.circle, color: ColorHelper.lightDark),
                child: Icon(Icons.notifications_outlined, color: ColorHelper.white),
              ),
            ),
          )
        ],
      ),
      body: const CarDetailPage(),
    );
  }

}
