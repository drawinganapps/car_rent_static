import 'package:car_rent_preview/helper/color_helper.dart';
import 'package:car_rent_preview/views/pages/home_page.dart';
import 'package:car_rent_preview/views/widgets/navigation_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorHelper.dark,
        elevation: 0,
        leadingWidth: widthSize * 0.2,
        leading: Center(
            child: Container(
          padding: const EdgeInsets.all(7),
          decoration: BoxDecoration(
              color: ColorHelper.lightDark, shape: BoxShape.circle),
          child: Icon(Icons.menu_rounded, color: ColorHelper.white),
        )),
        centerTitle: true,
        title: Text('Home',
            style: TextStyle(
                color: ColorHelper.white,
                fontSize: 20,
                fontWeight: FontWeight.w600)),
        actions: [
          Container(
            margin: EdgeInsets.only(right: widthSize * 0.05),
            child: Center(
              child: Container(
                padding: const EdgeInsets.all(7),
                decoration: BoxDecoration(
                  color: ColorHelper.lightDark,
                  shape: BoxShape.circle
                ),
                child: Icon(Icons.notifications_outlined, color: ColorHelper.white),
              ),
            ),
          )
        ],
      ),
      body: const HomePage(),
      bottomNavigationBar: const NavigationWidget(selectedMenu: 0),
    );
  }
}
