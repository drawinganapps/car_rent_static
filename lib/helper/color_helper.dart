import 'package:flutter/material.dart';

class ColorHelper {
  static Color primary = const Color.fromRGBO(244, 183, 85, 1);
  static Color secondary = const Color.fromRGBO(43, 38, 38, 1.0);
  static Color dark = const Color.fromRGBO(17, 23, 30, 1);
  static Color lightDark = const Color.fromRGBO(39, 45, 52, 1);
  static Color white = const Color.fromRGBO(255,255,255, 1);
  static Color grey = const Color.fromRGBO(110, 110, 110, 1);
}
