import 'package:car_rent_preview/models/brand_model.dart';

class CarModel {
  int id;
  String name;
  double rentPrice;
  String icon;
  double ratings;
  BrandModel brand;
  String description;

  CarModel(
      {required this.id,
      required this.name,
      required this.rentPrice,
      required this.icon,
      required this.ratings,
      required this.brand,
      required this.description});
}
