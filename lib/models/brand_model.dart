class BrandModel {
  int id;
  String name;
  String icon;

  BrandModel({required this.id, required this.name, required this.icon});
}
